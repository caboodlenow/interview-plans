## Senior Software Engineer - Interview plan

### Meet and Greet

**Duration:** 30 minutes

**Email template**
<!-- Include email to send to candidate -->

**Questions:**

1. What are your salary expectations?
1. What is the reason for a change?

----

### Deep-dive: Craft and process

**Email template**
<!-- Include email to send to candidate -->

**Duration:** 30 minutes

**Questions:**

1. In your previous experience working in teams -
  1. How did the team (Product, Engineering and Design) work together?
  1. How did the team know that they were building the right thing and how did they know they were done?
  1. How do you determine the success of what you built?
 *(Look for examples indicating a definition of done for ever task they ship, active communication with PM to help with prioritisation and status update)*

1. What type of quality standards would you apply to production code?

 *(Look for examples like unit and integration tests, documentation, version control, monitoring and debuggability)*

1. Tell me about a mistake you made,
  1. How did you deal with it?
  1. If it were up to you, what would you have changed?
 *(Look for examples like systems being on fire, inadequate monitoring or alerting, bad performing code or memory leaks)*

1. How do you approach balancing time vs scope vs quality/tech debt vs cost?

 *(Look for examples indicating they move fast applying the right level of quality standards. Rigid testing is not always good)*

----

### Take home task

**Email template**
<!-- Include email to send to candidate -->

**Instructions**

Zip the contents in `take-home-task` folder in this directory and share it with the candidate.

----

### Take home task discussion

**Duration:** 45 minutes
