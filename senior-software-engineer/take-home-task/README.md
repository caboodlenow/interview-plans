# Devicedesk coding challenge

## Background
You have been given three files to create a program for calculating the price of a cart. You may choose PHP, Ruby or Javascript for your submission.

- **A cart file.** This is a sample cart file that you can use to test your program.
- **A shipping rules file.** This file includes all the rules that could be applied at the time of calculating the cart total.
- **A catalogue.** This file includes a list of all products available for purchase along with their prices in dollars.

## The problem
Your program should take a file as a command line argument. A JSON file representing a cart. A sample cart is shared with this challenge as an example (See `cart-306.json`).

Your program should output the total price of the cart in dollars. The prices for products are available in `catalogue.json`. As an aid to test your program, the sample cart filename includes the expected total price for that cart.

When calculating the total price of the cart, your program must consider shipping rules and include a shipping price in the total. These shipping rules are available in `shipping-rules.json`. Note that the shipping rules apply only to those products that have `hasWeight` set to true and therefore does not apply to the total quantity of the cart.

## Things to note
- The time your program takes to calculate a price should be constant with respect to the number of products in the catalogue. In reality, the catalogue can have thousands of products.
- Your program should gracefully handle missing information.
- Include automated tests for your code.
- When you setup your submission, make sure you include documentation to make it easy for us to review, e.g., instructions to install your program, run it and run tests.
- Treat this code as production quality code i.e., it’s clean, clear, well refactored and one that you feel proud to commit at work or to an open source project.
- Your code should reflect and explain the problem domain.
- While using versioning, use logical commits as applicable.

## Submission guidelines
- At any point, if you have trouble understanding the requirements, please don’t hesitate to reach out to us.
- This submission is not timed. You can take as much time as you like, however we understand that your time is valuable and this test is designed in such a way that it doesn’t take more than a few hours.
- When you’re done with the challenge, please respond with a zipped file of your project (including git configuration) or a github link to your repository.
