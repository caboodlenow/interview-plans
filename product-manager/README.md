## Product Manager - Interview plan

### Meet and Greet

**Duration:** 30 minutes

**Email template**
<!-- Include email to send to candidate -->

**Questions:**

1. What are your salary expectations?
1. What is the reason for a change?

----

### Deep-dive: Craft and process

**Email template**
<!-- Include email to send to candidate -->

**Duration:** 30 minutes

**Questions:**

<!-- Include questions -->

----

### Take home task

**Email template**
<!-- Include email to send to candidate -->

**Instructions**

Export [Take-home task: Enhancing Devicedesk’s order approval capability](https://docs.google.com/document/d/1ODCH1Mq6srlYe8h4ns9bN6VYBdXg8sOzHZz5DZIhMNU/edit#) as PDF and send it to candidate.

----

### Take home task discussion

**Email template**
<!-- Include email to send to candidate -->

**Duration:** 45 minutes
